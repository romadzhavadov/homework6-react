import React from "react";
import Button from "./Button";
import { fireEvent, render, screen } from "@testing-library/react";


describe('Smoke test (Snapshot test', () => {
    test('Should Button render', () => {
        const { asFragment } = render(<Button>Click</Button>)
        
        
    expect(asFragment()).toMatchSnapshot()
    })
})

describe('Button click', () => {

    const handleClickMock = jest.fn()

    test('Should onClick triggering on Button click', () => {
        render(<Button onClick={handleClickMock} className="btn">Click</Button>)
        
        screen.debug();

        fireEvent.click(screen.getByRole('button'));

        expect(handleClickMock).toHaveBeenCalled();

    })


})