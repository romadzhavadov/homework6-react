import React from "react";
import ChoosenCard from "./ChoosenCard"
import { render } from "@testing-library/react";
import { Provider } from 'react-redux';
import store from "../../redux/store";

describe('Smoke test (Snapshot test', () => {
    test('Should ChosenCard render', () => {
        const { asFragment } = render(
            <Provider store={store}>
                <ChoosenCard name="name" price='price' url='url' id='id' color='color' isFavourite='isFavourite' count='count' />
            </Provider>
        )
        
    expect(asFragment()).toMatchSnapshot()
    })
})