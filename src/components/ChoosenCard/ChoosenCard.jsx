import React from "react";
import styles from "./ChoosenCard.module.scss";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import classNames from "classnames";
import { useDispatch, useSelector } from "react-redux";
import { setIsOpenModal, setHeaderModal, setTextModal, addActionsModal } from "../../redux/actionCreators/modalActionCreators";
import { removeToChosenGoods } from "../../redux/actionCreators/chosenGoodsAction";


const ChoosenCard = (props) => {
    const { url, name, price, color, id, count, isFavourite} = props;
    
    const dispatch = useDispatch()
    const {isOpen} = useSelector(state => state.modal)

    const clickToggleModal = (e) => {
        dispatch(setHeaderModal('Do you want to delete this Laptop?'))
        dispatch(setTextModal(`When you click OK, the laptop will be removed from the basket.`))
        dispatch(addActionsModal(
            <>
                <button className='btnActions' onClick={handleClickToRemove}>Ok</button>
                <button className='btnActions' onClick={()=>{dispatch(setIsOpenModal(false))}}>Cancel</button>
            </>))
        
        if(!isOpen) {
          dispatch(setIsOpenModal(true))
        } else {
          dispatch(setIsOpenModal(false))
        }
    }

    const handleClickToRemove = (e) => {
        const newState = {name, price, url, id, color, isFavourite, count}
        dispatch(removeToChosenGoods(newState));
        dispatch(setIsOpenModal(false))
    }

    return(
        <div className={styles.wrapper}>
            <div className={styles.imageWrap}>
                <img className={styles.img} src={url} alt={name} />
            </div>
            <p className={styles.caption}>{name}</p>
            <div className={styles.infoWrap}>
                <span className={styles.info}>your price: {price} $</span>
                <span className={styles.desc}> color: {color}</span>
            </div>
            <p className={styles.info}>Number: {count}</p>
            <Button onClick={clickToggleModal} className={classNames(styles.btn)}>Delete Laps</Button>
        </div>
    )

}

ChoosenCard.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    url: PropTypes.string,
    color: PropTypes.string,
    count: PropTypes.number,
    id: PropTypes.number,
    isFavourite: PropTypes.bool
}

ChoosenCard.defaultProps = {
    name: 'text',
    price: 'text',
    url: 'text',
    count: 0,
    color: 'text',
    id: 0,
    isFavourite: false,

}


export default ChoosenCard;