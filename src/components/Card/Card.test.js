import React from "react";
import Card from "./Card";
import { render } from "@testing-library/react";
import { Provider } from 'react-redux';
import store from "../../redux/store";

describe('Smoke test (Snapshot test', () => {
    test('Should Card render', () => {
        const { asFragment } = render(
            <Provider store={store}>
                <Card name="name" price='price' url='url' id='id' color='color' isFavourite='isFavourite' />
            </Provider>
        )
        
        
    expect(asFragment()).toMatchSnapshot()
    })
})