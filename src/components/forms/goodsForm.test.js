import React from "react";
import GoodsForm from "./goodsForm"; 
import { render } from "@testing-library/react";
import { Provider} from 'react-redux';
import store from "../../redux/store";

describe('Smoke test (Snapshot test', () => {
    test('Should Forms render', () => {
        const { asFragment } = render(
            <Provider store={store}>
                <GoodsForm/>
            </Provider>
        )
        
        
    expect(asFragment()).toMatchSnapshot()
    })
})

