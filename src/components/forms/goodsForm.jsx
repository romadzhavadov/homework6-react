import React from "react";
import { Formik, Form } from "formik";
import validationSchema from "./validationSchema";
import Input from "../Input/Input";
import { useDispatch } from "react-redux";
import { removeToChosenGoodsFromLs } from "../../redux/actionCreators/chosenGoodsAction";
import styles from "../forms/goodsForm.module.scss"
import classNames from "classnames";


const GoodsForm = () => {   // Formik як провайдер => в {children} замість children функція  яка повертає  jsx(з  компонентом Form), аргумент функції великий обєкт form 
    
    const dispatch = useDispatch()

    let initialValues = {
        name:'',
        secondName: '',
        age: '',
        adress: '',
        tel:''
    }

    const onSubmit = (value, actions) => {  // value це  початковий обьект
        console.log('Форма заповнення', value)
        const savedChosenGoods = localStorage.getItem('chosenGoods');
        const buyGoods = JSON.parse(savedChosenGoods)
        console.log('Куплені товари', buyGoods)
        dispatch(removeToChosenGoodsFromLs([]));
        localStorage.removeItem('chosenGoods');
        actions.resetForm()
    }

    return(
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            {(form) => {
                return (
                    <Form className={styles.form}>
                        <h2>Контактні дані:</h2>
                        <Input id="name" name="name" labelText="Ім'я:"/>
                        <Input id="secondName" name="secondName" labelText="Призвіще:"/>
                        <Input id="age" type='number' name="age" labelText="Вік:"/>
                        <Input id="adress" name="adress" labelText="Адреса:"/>
                        <Input id="name" type='number' name="tel" labelText="Телефон:"/>

                        <button className={classNames(styles.btnSubmit, {[styles.disabled]: !form.isValid})} disabled={!form.isValid} type="submit" >Checkout</button>
                    </Form>
                )
            }}
        </Formik>
    )
}

export default GoodsForm;