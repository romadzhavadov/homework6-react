import { object, string, number } from 'yup'

const validationSchema = object({
    name: string().required('Поле є обв\'язковим до заповнення'),
    secondName: string().required('Поле є обв\'язковим до заповнення'),
    age: number().required('Поле є обв\'язковим до заповнення')
        .min(18, 'Мінімальний вік 18 років')
        .max(97, 'Максимальний вік 97 років'),
    adress: string().required('Поле є обв\'язковим до заповнення'),
    tel: number().required('Поле є обв\'язковим до заповнення'),
})

export default validationSchema;