import React from "react";
import styles from './Modal.module.scss'
import { useDispatch, useSelector } from "react-redux";
import { setIsOpenModal } from "../../redux/actionCreators/modalActionCreators";


const Modal = () => {

    const {isOpen, header, text, actions} = useSelector(state => state.modal)
    const dispatch = useDispatch()

    const closefunction = () => {
        dispatch(setIsOpenModal(false))
    }

    if (!isOpen) {
        return null
    }

    return(
        <div className={styles.wrapper} data-testid='root'>
            <div className={styles.modalBackground} onClick={closefunction} data-testid='background'></div>
            <div className={styles.modal}>
                <div className={styles.modalHeaderContainer}>
                    <h2 className={styles.modalTitle}>{header}</h2>
                   {<span className={styles.closeModalButton} onClick={closefunction} data-testid='close'>&times;</span>} 
                </div>
                <p className={styles.modalContent}>{text}</p>
                {actions}
            </div>
        </div>

    )
}


export default Modal;