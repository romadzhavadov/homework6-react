import React, {useEffect} from "react";
import Modal from "./Modal";
import { Provider, useDispatch} from 'react-redux';
import store from "../../redux/store";
import { render, screen, fireEvent} from "@testing-library/react";
import { setIsOpenModal, addActionsModal } from "../../redux/actionCreators/modalActionCreators"; 


const handleClickMock = jest.fn()

const Dispatcher = () => {
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(setIsOpenModal(true))
        dispatch(addActionsModal(
            <>
                <button className='btnActions' onClick={handleClickMock}>Ok</button>
                <button className='btnActions' onClick={()=>{dispatch(setIsOpenModal(false))}}>Cancel</button>
            </>))

    }, [])

    return null
}

const Component = () => {
    return(
        <Provider store={store}>
            <Dispatcher/>
            <Modal/>
        </Provider>
    ) 
}

describe('Smoke test (Snapshot test', () => {
    test('Should Modal render', () => {
        const { asFragment } = render(<Component/>)
        
        expect(asFragment()).toMatchSnapshot()
    })
})

describe('Functions work', () => {
    test('Should Modal close on close button click', () => {
        render(<Component/>)
        screen.debug()

        fireEvent.click(screen.queryByTestId('close'));

        expect(screen.queryByTestId('root')).not.toBeInTheDocument()
    })

    test('Should Modal close on click on background', () => {
        render(<Component/>)
        screen.debug()

        fireEvent.click(screen.queryByTestId('background'));

        expect(screen.queryByTestId('root')).not.toBeInTheDocument()
    })


    test('Should hendleYes fired by click button yes', () => {
        render(<Component/>)

        screen.debug()

        fireEvent.click(screen.getByText('Ok'));

        expect(handleClickMock).toHaveBeenCalled();
    })

    test('Should hendleCancel fired by click button cancel', () => {
        render(<Component/>)

        screen.debug()

        fireEvent.click(screen.queryByText('Cancel'));

        expect(screen.queryByTestId('root')).not.toBeInTheDocument()
    })

})  
