import React, {memo, useContext} from "react";
import Card from "../Card/Card";
import {shallowEqual, useSelector } from "react-redux";
import styles from "./itemsContainer.module.scss"
import ViewContext from "../../context/viewContext";
import classNames from "classnames";
import { ReactComponent as ColumnIcon } from "../../asset/column.svg";
import { ReactComponent as TableIcon } from "../../asset/table.svg";

const ItemsContainer = () => {

    const items = useSelector(state => state.goods.goodsArr, shallowEqual)

    const { view, tableView, columnView } = useContext(ViewContext)

    return(
        <section className={classNames(styles.cardWrap, {
                [styles.cardWrapTable]: view ==='table',
                [styles.cardWrapColumn]: view ==='column'  
            })}>
            
            <div className={styles.titleWrap}>
                <h2> Всі товари:</h2>
                <div className={styles.iconWrap}>
                    <TableIcon className={styles.icon} onClick={tableView}/>
                    <ColumnIcon className={styles.icon} onClick={columnView}/>
                </div>
            </div>

            {items && items.map(({name, price, url, id, color, isFavourite }) => <Card key={id} name={name} price={price} url={url} id={id} color={color} isFavourite={isFavourite} /> )}

        </section>
    )

}

export default memo(ItemsContainer);