import { SET_MODAL, SET_HEADER, SET_TEXT, ADD_ACTIONS } from "../actions/modalAction";

const initialValue = {
    isOpen: false,
    header: "",
    text: "",
    actions: null
}


const modalReducer = (state = initialValue, action) => {
    switch(action.type) {
        case SET_MODAL: {
            return {...state, isOpen: action.payload}
        }
        case SET_HEADER: {
            return {...state, header: action.payload}
        }

        case SET_TEXT: {
            return {...state, text: action.payload}
        }

        case ADD_ACTIONS: {
            return {...state, actions: action.payload}
        }
        default: return state
    }
}

export default modalReducer;