import modalReducer from "../modalReducer";
import { setIsOpenModal, setHeaderModal, setTextModal, addActionsModal} from "../../actionCreators/modalActionCreators";

describe('Modal Reducers tests', () =>{
    test('should modalReducer return change isOpen value', () => {
        expect(modalReducer(undefined, setIsOpenModal(true)))
        .toEqual({
            isOpen: true,
            header: "",
            text: "",
            actions: null
        })
    });

    test('should modalReducer return change Header value', () => {
        expect(modalReducer(undefined, setHeaderModal("Header")))
        .toEqual({
            isOpen: false,
            header: "Header",
            text: "",
            actions: null
        })
    });

    test('should modalReducer return change text value', () => {
        expect(modalReducer(undefined, setTextModal("Text")))
        .toEqual({
            isOpen: false,
            header: "",
            text: "Text",
            actions: null
        })
    });

    test('should modalReducer return change Actions value', () => {
        expect(modalReducer(undefined, addActionsModal(<div></div>)))
        .toEqual({
            isOpen: false,
            header: "",
            text: "",
            actions: <div></div>
        })
    });
})