import chosenReducer from "../choosenReducer";
import { ADD_TO_CHOSEN_GOODS, REMOVE_TO_CHOSEN_GOODS } from "../../actions/goodsActions";


describe('Chosen Reducers tests', () =>{
    test('should ADD_TO_CHOSEN_GOODS return chosen state', () => {
        const initialState = {
            choosenArr: []
        };
        const action = {
            type: ADD_TO_CHOSEN_GOODS,
            payload: { id: 1, name: 'laptop'}
        };
        const expectedState = {
            choosenArr: [{ id: 1, name: 'laptop', count: 1 }]
        };
        expect(chosenReducer(initialState, action)).toEqual(expectedState);
    });

    test('should REMOVE_TO_CHOSEN_GOODS return change chosen state', () => {
        const initialState = {
            choosenArr: [{ id: 1, name: 'laptop', count: 2 }]
        };
        const action = {
            type: REMOVE_TO_CHOSEN_GOODS,
            payload: { id: 1, name: 'laptop', count: 2 }
        };
        const expectedState = {
            choosenArr: [{ id: 1, name: 'laptop', count: 1 }]
        };
        expect(chosenReducer(initialState, action)).toEqual(expectedState);
    });

})