import goodsReducer from "../goodsRedecer";
import { setGoods } from "../../actionCreators/goodsActionCreators";
import { SET_FAVOURITE } from "../../actions/goodsActions";

describe('Goods Reducers tests', () =>{
    test('should goodsReducer return state', () => {
        expect(goodsReducer(undefined, setGoods( [{ id: 1, name: 'laptop' }])))
        .toEqual({
            goodsArr: [{ id: 1, name: 'laptop' }],
        })
    });

    test('should setFavouriteCard change favourite state', () => {
        const initialState = {
            goodsArr: [{ id: 1, name: 'laptop', isFavourite: false }]
        };
        const action = {
            type: SET_FAVOURITE,
            payload: 1
        };
        const expectedState = {
            goodsArr: [{ id: 1, name: 'laptop', isFavourite: true }]
        };
        expect(goodsReducer(initialState, action)).toEqual(expectedState);
    });

})