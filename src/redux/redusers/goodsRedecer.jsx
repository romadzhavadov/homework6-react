import { SET_GOODS, SET_FAVOURITE } from "../actions/goodsActions";

const initialValue = {
    goodsArr:  [],
}

const goodsReducer = (state = initialValue, actions) => {
    switch(actions.type) {

        case SET_GOODS: {
            return {...state, goodsArr: actions.payload}
        }

        case SET_FAVOURITE: {

            const newState = [...state.goodsArr]; 

            const index = newState.findIndex((item) => item.id === actions.payload) 
            if (index !== -1) { 
                const updatedItem = { ...newState[index], isFavourite: !newState[index].isFavourite };
                newState[index] = updatedItem;
            } 
            localStorage.setItem('favGoods', JSON.stringify(newState));
            return { ...state, goodsArr: newState };
        }

        default: return state
    }
}

export default goodsReducer;