import { ADD_TO_CHOSEN_GOODS, REMOVE_TO_CHOSEN_GOODS, REMOVE_CHOSEN_GOODS_FROM_LOCAL_STORAGE } from "../actions/goodsActions"

export const addToChosenGoods = (item) => ({type: ADD_TO_CHOSEN_GOODS, payload: item})
export const removeToChosenGoods = (item) => ({type: REMOVE_TO_CHOSEN_GOODS, payload: item})
export const removeToChosenGoodsFromLs = (item) => ({type: REMOVE_CHOSEN_GOODS_FROM_LOCAL_STORAGE, payload: item})


