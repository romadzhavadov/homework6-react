import React, {useState} from "react";
import ViewContext from ".";

const ViewContextProvider = ({children}) => {

    const [view, setView] = useState('table');

    // const toogleView = () => {
    //   setView(prev => prev === 'table' ? 'column' : 'table')
    // }

    const tableView = () => {
      setView('table')
    }

    const columnView = () => {
      setView('column')
    }
  
    const value = {
      view,
      tableView,
      columnView,
    }

    return(
        <ViewContext.Provider value={value}>
            {children}
        </ViewContext.Provider>
    )
}

export default ViewContextProvider;