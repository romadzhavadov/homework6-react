import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
// через  команду:  npm install react-error-boundary
import { ErrorBoundary } from "react-error-boundary";
// import ErrorBoundary from './components/ErrorBoundary';
import SomethingWentWrong from './components/SomethingWentWrong/SomethingWentWrong';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Provider store={store}>
      <ErrorBoundary fallback={<SomethingWentWrong />}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ErrorBoundary>
    </Provider>

    // <ErrorBoundary >  вариант 2 
    //     <App />
    // </ErrorBoundary>
);



