import React, { useEffect, memo, useState } from 'react';
import { useDispatch} from 'react-redux'
import Header from './components/Header/Header';
import AppRoutes from './AppRoates';
import './App.scss';
import { fetchGoods } from './redux/actionCreators/goodsActionCreators';
import Modal from './components/Modal/Modal';
import ViewContextProvider from './context/viewContext/ViewContextProvider';

function App() {

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fetchGoods())
  }, [dispatch])

  return (
    <ViewContextProvider>
      <Header />
      <AppRoutes />
      <Modal />
    </ViewContextProvider>
  )
}



export default memo(App);
